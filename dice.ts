export function dX(x) {
	return Math.floor(Math.random() * x) + 1
}

export function XdY(x, y) {
	const ret = 0
	for (let i = 0; i < x; i++)
		ret += dX(y)
	return ret
}
