import { dX } from './dice'

export class Table {
	private options = []

	constructor(list) {
		// TODO: Check all elements are same dimensionality

		for (let i = 0; i < list.length; i++) {
			// Initialising from a 1-dimensional array

			if (!Array.isArray(list[i])) {
				this.options[i] = list[i]
				continue
			}

			// Two-dimensional array

			const key = list[i][0]
			const val = list[i][1]

			// TODO: Check keys are in order

			if (typeof(key) === 'number') {
				this.options[key - 1] = val
				continue
			}

			if (typeof(key) === 'string') {
				let [ start, end ] = key.split('-')
				start = Number(start)
				end = Number(end)
				if (end === undefined)
					end = start

				for (let e = start; e <= end; e++)
					this.options[e - 1] = val
				continue
			}

			console.error('Unimplemented key type!', typeof(key))
		}
	}

	roll() {
		return this.entry(dX(this.options.length))
	}

	entry(n: number) {
		return this.options[n - 1]
	}
}
